open Login
open Printf

(* A simple command line program to send download tasks to a Freebox. *)

let name = "QuickDownload"
let id = "fbx-cli";;
let version = "0";;

let rec drop n l = match n, l with
  | n, _ :: t when n > 0 -> drop (n-1) t
  | _ -> l

type todl = {
  name : string; (* Name displayed on the command line. *)
  dl_dir : string; (* Download target directory. *)
}

let dltask name dl_dir = { name; dl_dir }
let default_task = dltask "Download" "/Disque dur/Téléchargements"
let forever x = let rec xs = x :: xs in xs

(* Make a [todl] task out of a string: ["name, /dl_dir"]. *)
let parse s =
  let open String in
  let i = index s ',' in
  { name = trim (sub s 0 i); dl_dir = trim (sub s (i+1) (length s - i - 1)) }

(* Read a list of tasks. *)
let read_task file =
  let h = open_in file in
  let rec rd () =
    try
      let s = input_line h in
      parse s :: rd ()
    with End_of_file -> []
  in rd ()

(* Do a task. Use name to identify the current task for the user,
  and read a URL from stdin. An empty line skips the task. *)
let dl_task k { name; dl_dir } =
  printf "%s> " name;
  let url = String.trim (read_line ()) in
  let dl_dir = Fs.encode dl_dir in
  if url <> "" then ignore (
    Download.add_task_by_url k ~dl_dir [url]
  )

(* The first argument of the program, [keyfile], contains an app token to
  authenticate with the Freebox.
  The following arguments are files, where each line contains two comma
  separated elements: the task name and the download directory.
  If there are no arguments besides [keyfile],
  infinitely many tasks are generated, targetting a default directory
  (/Disque dur/Téléchargements). *)
let () =
  let todl =
    if Array.length Sys.argv < 2 then (
      eprintf "usage: %s keyfile [dltask]*" Sys.argv.(0);
      exit 1)
    else if Array.length Sys.argv = 2 then forever default_task
    else List.concat
      (Sys.argv |> Array.to_list |> drop 2 |> List.map read_task) in
  let fn = Sys.argv.(1) in
  let k, p = login_file id version fn in
  try
    if not p.downloader then failwith "No downloading permission.";
    List.iter (dl_task k) todl;
    logout k
  with e -> logout k; if e <> End_of_file then raise e

