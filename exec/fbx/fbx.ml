open Login
open Printf

let name = "freeboxOS CLI"
let id = "fbx-cli";;
let version = "0";;

let json_of_perms perms = Yojson.(`Assoc [
  "settings", `Bool perms.settings;
  "contacts", `Bool perms.contacts;
  "calls", `Bool perms.calls;
  "explorer", `Bool perms.explorer;
  "downloader", `Bool perms.downloader;
  "parental", `Bool perms.parental;
  "pvr", `Bool perms.pvr;
])

let () =
  let fn = Sys.argv.(1) in
  let a_tok, c =
    if not (Sys.file_exists fn) then
      let cout = open_out fn in
      try
        let (a_tok, _) as ac = new_app_token id name version in
        output_string cout a_tok;
        close_out cout;
        ac
      with e -> printf "Error. Removing %s" fn; Sys.remove fn; raise e
    else
      let cin = open_in fn in
      let a_tok = input_line cin in
      let c = challenge () in
      close_in cin;
      (a_tok, c) in
  printf "App token: %s\n" a_tok;
  let s_tok, perms = open_session id version (password a_tok c) in
  printf "Logged in.\n";
  printf "Session: %s\n" s_tok;
  printf "%s\n" (perms |> json_of_perms |> Yojson.to_string);
  logout s_tok;
  printf "Logged out.\n"

