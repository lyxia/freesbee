OCaml bindings to the FreeboxOS API
===

The Freebox is an ADSL modem provided by the French ISP Free.

[FreeboxOS API documentation.](http://dev.freebox.fr/sdk/os/)

Dependencies
---

(I am only indicating what I am using in my current build.
This almost certainly compiles with much older versions.)

- `ocaml (>= 4.01)`
- [`ocamlnet (>= 4.0.1)`](https://opam.ocaml.org/packages/ocamlnet/ocamlnet.4.0.1/)
- [`yojson (>= 1.2)`](https://opam.ocaml.org/packages/yojson/yojson.1.2.0/)
- [`cryptokit (>= 1.10)`](https://opam.ocaml.org/packages/cryptokit/cryptokit.1.10/)
- `hex`, `base64`

Building with `oasis`
---

Complicated stuff. A fixed `setup.ml` shall be distributed once
the library has stabilized a bit.

    oasis setup
    ocaml setup.ml -configure
    ocaml setup.ml -build
    ocaml setup.ml -install

