(** FreeboxOS-specific HTTP calls *)

open Nethttp_client
open Yojson.Basic
open Url

(** {2 Low level calls} *)

(** The optional [key] string allows to make authenticated calls,
  and the [fbx] URL (a string) specifies the address of the target Freebox,
  the default being ["http://mafreebox.freebox.fr"]. *)

class get : ?fbx:url -> ?key:string -> string -> http_call
class post : ?fbx:url -> ?key:string -> string -> (string * string) list
  -> http_call
class post_raw : ?fbx:url -> ?key:string -> string -> string -> http_call
class put : ?fbx:url -> ?key:string -> string -> string -> http_call
class delete : ?fbx:url -> ?key:string -> string -> http_call

(** {2 Wrappers} *)

val request : http_call -> string
val unpack_result : string -> json

val get' : ?fbx:url -> ?key:string -> string -> json
val delete' : ?fbx:url -> ?key:string -> string -> json
val post_url : ?fbx:url -> ?key:string -> string -> (string * string) list -> json
val post_json : ?fbx:url -> ?key:string -> string -> json -> json
val put_json : ?fbx:url -> ?key:string -> string -> json -> json

val post_multipart : ?fbx:url -> ?key:string -> string ->
  Netmime.complex_mime_message -> json

val api_version : ?fbx:url -> unit -> json

