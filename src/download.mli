(** Freebox Download API *)

open Login
open Yojson.Basic
open Url

type download = json
type task_id = int

(** Return all tasks. *)
val task_list : ?fbx:url -> session_token -> download list

(** Return the task with the given ID. *)
val task : ?fbx:url -> session_token -> task_id -> download

(** Delete the download task with the given ID,
  without erasing the downloaded files. *)
val delete : ?fbx:url -> session_token -> task_id -> unit

(** Erases the downloaded files. *)
val remove : ?fbx:url -> session_token -> task_id -> unit

(** Update task. *)
val update : ?fbx:url -> session_token -> task_id -> (string * json) list ->
  download

val task_log : ?fbx:url -> session_token -> task_id -> string

val add_task_by_url : ?fbx:url -> session_token -> ?recursive:bool ->
  ?username:string -> ?password:string ->
    ?archive_password:string -> ?cookies:string ->
      ?dl_dir:Fs.path -> url list -> task_id list

val add_task_by_file : ?fbx:url -> session_token ->
  ?archive_password:string -> ?dl_dir:Fs.path -> string (* Filename *)
  -> task_id

