exception RequestError of string * string
exception InvalidToken
exception AuthTimeout
exception AuthDenied
