(** Freebox File System API *)

open Login
open Yojson.Basic
open Url

(** Filepaths are encoded in base64. *)

type path = string

val encode : string -> path
val decode : path -> string

(** File information as returned by [ls]. *)
type file_info = {
  path : string;
  name : string;
  mimetype : string;
  is_file : bool; (** [false] if directory *)
  size : int;
  modification : int;
  index : int;
  link : string option; (** symlink *)
  hidden : bool;
  foldercount : int option;
  filecount : int option;
}

val file_info_of_json : ?count:bool -> json -> file_info

(** List files in a directory. *)
val ls : ?fbx:url -> session_token -> path -> file_info list

(** Get file info. *)
val info : ?fbx:url -> session_token -> path -> file_info

