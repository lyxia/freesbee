open Http
open Utils
open Yojson.Basic.Util

type download = Yojson.Basic.json
type task_id = int

let to_task j = j

let downloads = "downloads/"

let task_list ?fbx key =
  get' ?fbx ~key downloads |> to_list |> List.map to_task

let task ?fbx key i =
  get' ?fbx ~key (downloads ^ string_of_int i) |> to_task

let delete ?fbx key i =
  delete' ?fbx ~key (downloads ^ string_of_int i) |> ignore

let remove ?fbx key i =
  delete' ?fbx ~key (downloads ^ string_of_int i ^ "/erase/") |> ignore

let update ?fbx key i j =
  put_json ?fbx ~key (downloads ^ string_of_int i) (`Assoc j)
  |> to_task

let task_log ?fbx key i =
  get' ?fbx ~key (downloads ^ string_of_int i ^ "/log") |> to_string

let (&?) a = map_option (fun b -> (a, b))

let add_task_by_url ?fbx key ?recursive ?username ?password
  ?archive_password ?cookies ?dl_dir urls =
    let dl_url, single = match urls with
      | [url] -> ("download_url", url), true
      | url :: urls ->
          ("download_urls",
          List.fold_left (fun us u -> us ^ "\n" ^ u) url urls),
          false
      | [] -> invalid_arg "No download URL."
    in
    let p = dl_url :: cat_options [
      "recursive" &? map_option string_of_bool recursive;
      "username" &? username;
      "password" &? password;
      "archive_password" &? archive_password;
      "cookies" &? cookies;
      "download_dir" &? dl_dir;
    ] in
    let r = post_url ?fbx ~key (downloads ^ "add") p |> member "id" in
    if single then [r |> to_int] else r |> to_list |> List.map to_int

let add_task_by_file ?fbx key ?archive_password ?dl_dir fn = Netmime.(
  let disposition = "Content-Disposition" in
  let form_data n = "form-data; name=\"" ^ n ^ "\"" in
  let hdr = basic_mime_header ["Content-Type", "multi-part/form-data"] in
  let ap_hdr = basic_mime_header [disposition, form_data "archive_password"] in
  let dd_hdr = basic_mime_header [disposition, form_data "download_dir"] in
  let fn_hdr = basic_mime_header
    [disposition, form_data "download_file" ^ "; filename=\"" ^ fn ^ "\"";
    "Content-Type", "application/x-bittorrent"] in
  let body = `Parts (cat_options [
    archive_password |?> (fun ap -> ap_hdr, `Body (memory_mime_body ap));
    dl_dir |?> (fun dd -> dd_hdr, `Body (memory_mime_body dd));
    Some (fn_hdr, `Body (file_mime_body fn))]) in
  post_multipart ?fbx ~key (downloads ^ "add") (hdr, body) |> to_int)

