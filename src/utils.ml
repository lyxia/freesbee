let (|?) x y =
  match x with
  | None -> y
  | Some z -> z

let map_option f = function
  | None -> None
  | Some x -> Some (f x)

let rec cat_options = function
  | None :: t -> cat_options t
  | Some y :: t -> y :: cat_options t
  | [] -> []

let (|?>) x f = map_option f x

