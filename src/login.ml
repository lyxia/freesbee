(* Gain access to Freebox API

  http ://dev.freebox.fr/sdk/os/login/
 *)

open Error
open Http
open Utils
open Yojson.Basic.Util

type app_token = string
type track_id = int

type authorize_status =
  | Unknown
  | Pending
  | Timeout
  | Granted
  | Denied

type app_permissions = {
  settings : bool;
  contacts : bool;
  calls : bool;
  explorer : bool;
  downloader : bool;
  parental : bool;
  pvr : bool;
}

type session_token = string

let login = "login/"

let app_request ?fbx ?device_name ~id ~name ~version =
  let tkrq = `Assoc [
    "app_id", `String id;
    "app_name", `String name;
    "app_version", `String version;
    "device_name", `String (device_name |? Unix.gethostname ())
  ] in
  let result = post_json ?fbx (login ^ "authorize/") tkrq in
  let app_token = result |> member "app_token" |> to_string in
  let track_id = result |> member "track_id" |> to_int in
  (app_token, track_id)

let string_to_status x = List.assoc x [
  "unknown", Unknown;
  "pending", Pending;
  "timeout", Timeout;
  "granted", Granted;
  "denied", Denied]

let track_id ?fbx tid =
  let result = get' ?fbx (login ^ "authorize/" ^ string_of_int tid) in
  let status = result |> member "status" |> to_string |> string_to_status in
  let challenge = result |> member "challenge" |> to_string in
  (status, challenge)

let default_permissions = {
  settings = false;
  contacts = false;
  calls = false;
  explorer = false;
  downloader = false;
  parental = false;
  pvr = false;
}

let logged_in' ?fbx ?key () =
  let result = get' ?key ?fbx login in
  let logged_in = result |> member "logged_in" |> to_bool in
  let challenge = result |> member "challenge" |> to_string in
  (logged_in, challenge)

let logged_in ?fbx key =
  let li, _ = logged_in' ~key ?fbx () in li

let challenge ?fbx () =
  let _, c = logged_in' ?fbx () in c

let assoc_true x l =
  try List.assoc x l |> to_bool with Not_found -> false

let to_permissions j =
  let p = to_assoc j in
  {
    settings = assoc_true "settings" p;
    contacts = assoc_true "contacts" p;
    calls = assoc_true "calls" p;
    explorer = assoc_true "explorer" p;
    downloader = assoc_true "downloader" p;
    parental = assoc_true "parental" p;
    pvr = assoc_true "pvr" p;
  }

let open_session ?fbx ~id ~version ~password =
  let j = `Assoc [
    "app_id", `String id;
    "app_version", `String version;
    "password", `String password
  ] in
  let result = post_json ?fbx (login ^ "session/") j in
  let s_tok = result |> member "session_token" |> to_string in
  let permissions = result |> member "permissions" |> to_permissions in
  (s_tok, permissions)

let logout ?fbx key = ignore (post_url ~key ?fbx (login ^ "logout/") [])

let password app_tok c =
  let h = Cryptokit.hash_string (Cryptokit.MAC.hmac_sha1 app_tok) c in
  Hex.(match of_string h with `Hex s -> s)

(** Wrappers *)

let new_app_token ?fbx ?device_name ~id ~name ~version =
  let t, tid = app_request ?fbx ?device_name ~id ~name ~version in
  let rec monitor () =
    match track_id tid with
    | Pending, _ -> Unix.sleep 2; monitor ()
    | Granted, c -> (t, c)
    | Unknown, _ -> raise InvalidToken
    | Timeout, _ -> raise AuthTimeout
    | Denied, _ -> raise AuthDenied
  in monitor ()

let login ?fbx ~id ~version ~key c =
  open_session ?fbx ~id ~version ~password:(password key c)

let new_token_file ?fbx ?device_name ~id ~name ~version file =
  if Sys.file_exists file
  then failwith ("File \"" ^ file ^ "\"exists already.");
  let key, _ = app_request ?fbx ?device_name ~id ~name ~version in
  let cout = open_out file in
  output_string cout key;
  close_out cout

let login_file ?fbx ~id ~version file =
  let cin = open_in file in
  let key = input_line cin in
  close_in cin;
  let c = challenge ?fbx () in
  login ?fbx ~id ~version ~key c

