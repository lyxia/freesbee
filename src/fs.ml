open Http
open Yojson.Basic.Util

type path = string

(* Use the B64 defaults *)
let encode s = B64.encode s
let decode s = B64.decode s

type file_info = {
  path : string;
  name : string;
  mimetype : string;
  is_file : bool; (** [false] if directory *)
  size : int;
  modification : int;
  index : int;
  link : string option; (** symlink *)
  hidden : bool;
  foldercount : int option;
  filecount : int option;
}

let fs = "fs/"

let file_info_of_json ?(count = false) j = {
  path = j |> member "path" |> to_string;
  name = j |> member "name" |> to_string;
  mimetype = j |> member "mimetype" |> to_string;
  is_file = j |> member "type" |> to_string = "file";
  size = j |> member "size" |> to_int;
  modification = j |> member "modification" |> to_int;
  index = j |> member "index" |> to_int;
  link = if j |> member "link" |> to_bool
    then Some (j |> member "target" |> to_string)
    else None;
  hidden = j |> member "hidden" |> to_bool;
  foldercount = if count
    then Some (j |> member "foldercount" |> to_int)
    else None;
  filecount = if count
    then Some (j |> member "filecount" |> to_int)
    else None;
}

let ls ?fbx key path =
  get' ?fbx ~key (fs ^ "ls/" ^ path) |> to_list |> List.map file_info_of_json

let info ?fbx key path =
  get' ?fbx ~key (fs ^ "info/" ^ path) |> file_info_of_json

