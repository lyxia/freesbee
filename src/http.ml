open Json
open Nethttp_client
open Url
open Yojson.Basic

let authenticate key m =
  match key with
  | Some k -> (m # request_header `Base) # update_field "X-Fbx-App-Auth" k
  | None -> ()

let set_content_type m s =
  (m # request_header `Base) # update_field "Content-Type" s

let update_header m hdr =
  let rec partition_by k = function
    | [] -> [], []
    | (k', v) as h :: t ->
        let vs, t' = partition_by k t in
        if k = k' then v :: vs, t' else vs, h :: t'
  in
  let rec nub = function
    | [] -> []
    | (k, v) :: t ->
        let vs, t' = partition_by k t in
        (k, v :: vs) :: nub t'
  in
  let m_hdr = m # request_header `Base in
  List.iter
    (fun (f, v) -> m_hdr # update_multiple_field f v)
    (nub (hdr # fields))

class get ?(fbx=default_api) ?key rsrc =
  object (self)
    inherit Nethttp_client.get (fbx // rsrc)
    initializer authenticate key self
  end

class post ?(fbx=default_api) ?key rsrc body =
  object (self)
    inherit Nethttp_client.post (fbx // rsrc) body
    initializer authenticate key self
  end

class post_raw ?(fbx=default_api) ?key rsrc body =
  object (self)
    inherit Nethttp_client.post_raw (fbx // rsrc) body
    initializer authenticate key self
  end

class put ?(fbx=default_api) ?key rsrc body =
  object (self)
    inherit Nethttp_client.put (fbx // rsrc) body
    initializer authenticate key self
  end

class delete ?(fbx=default_api) ?key rsrc =
  object (self)
    inherit Nethttp_client.delete (fbx // rsrc)
    initializer authenticate key self
  end

let unpack_result x = unpack (from_string x)

(* Probably thread unsafe. *)
let request m =
  let p = new Nethttp_client.pipeline in
  try
    p # add m;
    p # run ();
    m # response_body # value
  with e -> p # reset () ; raise e

let unpack_request m = unpack_result (request m)

let break_complex_mime_message mm = Netchannels.(
  let ch = new pipe () in
  Netmime_channels.write_mime_message (ch :> out_obj_channel) mm;
  ch # close_out ();
  let strm = new Netstream.input_stream (ch :> in_obj_channel) in
  let hdr = Netmime_channels.read_mime_header strm in
  let body = string_of_in_obj_channel (strm :> in_obj_channel) in
  strm # close_in ();
  (hdr, body))

let get' ?fbx ?key rsrc = unpack_request (new get ?fbx ?key rsrc)

let delete' ?fbx ?key rsrc = unpack_request (new delete ?fbx ?key rsrc)

let post_url ?fbx ?key rsrc body = unpack_request (new post ?fbx ?key rsrc body)

let post_json ?fbx ?key rsrc body =
  let m = new post_raw ?fbx ?key rsrc (Yojson.Basic.to_string body) in
  set_content_type m "application/json";
  unpack_request m

let put_json ?fbx ?key rsrc body =
  let m = new put ?fbx ?key rsrc (Yojson.Basic.to_string body) in
  set_content_type m "application/json";
  unpack_request m

let post_multipart ?fbx ?key rsrc mm =
  let hdr, body = break_complex_mime_message mm in
  let m = new post_raw ?fbx ?key rsrc body in
  update_header m hdr;
  unpack_request m

let api_version ?(fbx=my_freebox) () =
  from_string (Convenience.http_get (fbx // "api_version"))

