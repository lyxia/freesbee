open Error
open Yojson.Basic.Util

let success ans = ans |> member "success" |> to_bool
let result ans = ans |> member "result"
let error_code ans = ans |> member "error_code" |> to_string
let msg ans = ans |> member "msg" |> to_string

let unpack ans =
  if success ans then result ans
  else raise (RequestError (error_code ans, msg ans))

