(** Gaining access to Freebox API

   {{:http ://dev.freebox.fr/sdk/os/login/}FreeboxOS Login API}
 *)

open Yojson.Basic
open Url

(** {2 Base API} *)

type app_token = string
type track_id = int
type session_token = string

(** Obtain an app token.

  After calling this function, get up and check your Freebox.

  @param device_name Name of the device. Default is hostname.
  @param id App identifier.
  @param name App name.
  @param version App version. *)
val app_request : ?fbx:url
  -> ?device_name:string -> id:string -> name:string -> version:string
  -> app_token * track_id

type authorize_status =
  | Unknown
  | Pending
  | Timeout
  | Granted
  | Denied

(** Check whether the token has been approved. *)
val track_id : ?fbx:url -> track_id -> authorize_status * string (* challenge *)

(** Request the current challenge.

  Returns:
  a boolean indicating whether the given key corresponds to an active session,
  the current challenge string. *)
val logged_in' : ?fbx:url -> ?key:session_token -> unit -> bool * string

val logged_in : ?fbx:url -> session_token -> bool
val challenge : ?fbx:url -> unit -> string

type app_permissions = {
  settings : bool;
  contacts : bool;
  calls : bool;
  explorer : bool;
  downloader : bool;
  parental : bool;
  pvr : bool;
}

(** Answer the challenge to log in.

  [password = hmac-sha1(app_token, challenge)] *)
val open_session : ?fbx:url ->
  id:string -> version:string -> password:string ->
  session_token * app_permissions

(** Close the current session. *)
val logout : ?fbx:url -> session_token -> unit

(** [password app_token challenge], returns the corresponding
  password (hmac-sha1 in hex). *)
val password : app_token -> string (* challenge *) -> string

(** {2 Extensions} *)

(** Monitor the newly obtained token every two seconds until access has been
  granted before returning the token and the current challenge. *)
val new_app_token : ?fbx:url ->
  ?device_name:string -> id:string -> name:string -> version:string ->
  app_token * string

(** Wrapper around [open_session] and [password]. *)
val login : ?fbx:url -> id:string -> version:string ->
  key:app_token -> string -> session_token * app_permissions

(** Get a new app token and write it in a file. *)
val new_token_file : ?fbx:url ->
  ?device_name:string -> id:string -> name:string -> version:string ->
    string -> unit

(** Read an app token from a file and log in. *)
val login_file : ?fbx:url -> id:string -> version:string -> string ->
  session_token * app_permissions

